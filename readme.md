# lettuce

An exploration of `tf-lattice`. The idea sounds neat but the code 
could sure be a lot simpler. 

## installation 

Install `lettuce` in the virtual environment via:

```bash
$ pip install --editable .
```
