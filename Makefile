flake:
	flake8 lettuce
	flake8 tests

install:
	pip install -e .

develop:
	python setup.py develop

test:
	pytest
