import os

import pandas as pd


CSV_COLUMNS = [
    "age", "workclass", "fnlwgt", "education", "education_num",
    "marital_status", "occupation", "relationship", "race", "gender",
    "capital_gain", "capital_loss", "hours_per_week", "native_country",
    "income_bracket"
]


def fetch_data():
    train_path = "/tmp/uci_census/adult.data"
    valid_path = "/tmp/uci_census/adult.test"
    if not os.path.exists("/tmp/uci_census"):
        os.mkdir("/tmp/uci_census")
    if not os.path.exists(valid_path):
        df_test = pd.read_csv("https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.test")
        df_test.to_csv(valid_path)
    if not os.path.exists(train_path):
        df_train = pd.read_csv("https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data")
        df_train.to_csv(train_path)
    df_train = pd.read_csv(train_path, names=CSV_COLUMNS,
                           skipinitialspace=True, engine="python", skiprows=1)
    df_valid = pd.read_csv(valid_path, names=CSV_COLUMNS,
                           skipinitialspace=True, engine="python", skiprows=1)
    return df_train, df_valid
